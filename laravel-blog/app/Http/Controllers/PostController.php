<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        //if there is an authenticated user
        if(Auth::user()){
            //create a new Post object from the Post model
            $post = new Post;

            //define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            //save the post object to the database
            $post->save();

            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from the database
        $posts = Post::where('user_id', '!=', 1)->get();
        $posts2 = Post::where('user_id', 1)->get();
        return view('posts.index')->with('posts', $posts2)->with('posts2', $posts);
    }
}
